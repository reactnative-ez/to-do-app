import React, { Component } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Button } from 'react-native';

import Header from './Header';
import Body from './Body';

class App extends Component {
  constructor() {
    super();

    this.state = {
      tareas: [],
      texto: '',
    };
  }

  changeText = (value) => {
    console.log('value -> ', value);
    this.setState({
      texto: value,
    });
  }

  agregarTarea = () => {
    const prueba = Date.now();
    console.log('date ->', prueba.toString())

    this.setState({
      tareas: [...this.state.tareas, { texto: this.state.texto, key: Date.now() }],
      texto: '',
    });
  }

  eliminarTarea = (id) => {
    const newTareas = this.state.tareas.filter((tarea) => {
      return tarea.key !== id;
    });

    this.setState({
      tareas: newTareas
    });
  }

  guardarTareas = () => {
    const tareas = this.state.tareas;

    AsyncStorage.setItem('@AppCursoUdemy:arrayUno',JSON.stringify(tareas))
      .then((val) => {
        console.log(val);
      })
      .catch((err) => {
        console.log(err);
      })
  }

  recuperarTareas = () => {
    AsyncStorage.getItem('@AppCursoUdemy:arrayUno')
      .then((val) => {
        console.log(val);
      })
      .catch((err) => {
        console.log(err);
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          changeText={this.changeText}
          agregar={this.agregarTarea}
          texto={this.state.texto}
        />
        <Button
          title="Guardar"
          onPress={() => {this.guardarTareas()}}
        />
        <Button
          title="Recuperar"
          onPress={() => { this.recuperarTareas() }}
        />
        <Body
          tareas={this.state.tareas}
          eliminar={this.eliminarTarea}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default App;
